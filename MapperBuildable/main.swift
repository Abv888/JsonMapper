import Foundation

let json = ["test": ["hello world", "it's test example", 1.1] as [Any]] as AnyObject

let string: [String] = try Mapper.map(for: "test", json)

print(string)

