import Foundation

public extension Data {
    func json() throws -> AnyObject {
        return try JSONSerialization.jsonObject(with: self, options: .mutableLeaves) as AnyObject
    }
}
